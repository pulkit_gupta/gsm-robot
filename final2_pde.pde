int greenLed=12;
int redLed = 11;

char inChar=0;   //temp variable for taking
int index = 0;   //total length of raw serial data
int refresh = 1500;  //time for polling
char curmsg='0';
int length_of_command=0;


char inMsg[200];   //serial raw message details storing
char command[100]; //Plain Message
char mbno[100]; // Senders Mobile No.


void greenOn(){digitalWrite(greenLed, HIGH);}
void greenOff(){digitalWrite(greenLed, LOW);}
void redOn(){digitalWrite(redLed, HIGH);}
void redOff(){digitalWrite(redLed, LOW);}


// Function to send any reply to Sender
void sendMsg(String no,char ms[])	
{
   Serial.print("AT+CMGS=\"");
// Serial.print("0");   9711606851
   Serial.print(no);
   Serial.println("\"");
   delay(1000);
   Serial.print(ms);
   Serial.write(26); // this is ctrl-z
}




void getCom()
{
  int var=0,i=0;
  for(i=0;i<100;i++)
  {
     command[i]='$'; 
  }
  for(i=0;i<index;i++)
  {
    if(inMsg[i]=='"')
    {
      var++;
    }
    if(var==6)
    {
      int j=0;
      i = i+1;
      for(j=0;j<index-i-3;j++)
       {
         command[j]=inMsg[i+j];                 
       }
       command[j]='\0';
       length_of_command=index-i-3;
       break;
    }
  }
  
}
void getNo()
{
  int flag=0;
  int i=0;  
  for(i=0;i<100;i++)
  {
     mbno[i]='_'; 
  }
  for(i=0;i<index;i++)
  {
    if(inMsg[i]=='1')
    {
      flag=1;
       for(int j=1;;j++)
       {
         mbno[j-1]=inMsg[i+j];
         if(mbno[j-1]=='"')
         {
           mbno[j-1]='\0';
           break;
         }
       }  
    }
    if(flag==1)
    {
      break;    
    }
  }
}

void deleteAll()
{
  delay(2000);
  Serial.flush();
  delay(2000);
  int k=1;
  for(k=1;k<9;k++)
  {
    Serial.print("AT+CMGD=");
    Serial.println(k);
    delay(2000);
  }
  
}
void readMsg()
{
        index=0;
        inChar=0;
  	for(int i=0;i<200;i++)
  	{
		inMsg[i]='_';
        }
  	Serial.print("AT+CMGR=");
        Serial.println(curmsg);
    	delay(1000);  //wait to search and display message from simcard
  	while(Serial.available() > 0)
  	{
          	  inChar = Serial.read(); // Read a character
                  if(!(inChar=='\n'))
                  {
                    inMsg[index] = inChar; // Store it
		    index++; // Increment w
                  }
         }
         inMsg[index]='\0';
         
        
        Serial.println(index,DEC);
	Serial.println(inMsg);  
        delay(3000);
        getNo();
        Serial.println(mbno);  
        getCom();
        Serial.println(command);  
        if(curmsg>=9)
        {
          //deleteAll();
        }
      
        action();
        
        delay(2000);
 	Serial.flush();
        delay(2000);
 }

boolean green()
{
  int i=0;
  for(i=0;command[i]!='\0';i++)
  {
    if(command[0]!='g')
    {
        Serial.print("false");
      return false;
    }
  }
  return true;
}


void action()
{
  Serial.print("commands");
  Serial.println(command);
  if(comp())
  {       
    greenOn();
    redOff();
    Serial.println("Green Matched");
  }
  else if(!comp())
  {       
    greenOff();
    redOn();
    Serial.println("RED Matched");
  }     
}

void msgPol()
{
//  Serial.println("Waiting for message");
  	while(Serial.available() > 0)
  	{
 	   if(Serial.read()==',')
     	   {
               curmsg = Serial.read(); 
               Serial.print("Message no. is:");
               Serial.println(curmsg);
               Serial.flush();
               delay(3000);
               readMsg();
               break;
	   }
	   index++; // Increment w
	}
}

void setup()
{  
  pinMode(greenLed, OUTPUT);
  pinMode(redLed, OUTPUT);
  redOn();
  greenOn();
  delay(1000);
  greenOff();
  // begin sending over serial port
  Serial.begin(9600);
  delay(100);
  Serial.println("ATE0");
  delay(100);
  greenOn();
  Serial.println("AT+CMGF=1");
  delay(1000);
  greenOff();
  Serial.flush();
  delay(1000);  
  greenOn();
}

void loop()
{
  
  msgPol();
  //readMsg();
  delay(refresh);

  //  Serial.print(vin);
  //  Serial.println(" volt");
}
